# gpu_calculator

## Description

With this app, you can calculate the time and money needed to render an image.

### Important

The image is a stub and is not taken into account in the calculations.

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

## Api documentation

Templates for all components

```
@/components/
```

Files for vuex

```
@/store/
```

Custom scripts

```
@/assets/scripts
```

Exported static scss files

```
@/assets/scss
```

Immutable data

```
@/assets/static
```

### Styles

-- All components have own scoped scss styles;

-- Component 'App' contains global styles for all project;
