module.exports = {
  publicPath:
    process.env.NODE_ENV === 'production'
      ? '/' + process.env.CI_PROJECT_NAME + '/'
      : '/',
  configureWebpack: {
    devtool: 'source-map',
  },
  css: {
    loaderOptions: {
      scss: {
        data: `
            @import "@/assets/scss/__variables.scss";
            @import "@/assets/scss/__mixins.scss";
          `,
      },
    },
  },
}
